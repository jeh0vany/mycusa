# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----

@auth.requires_login()
def index():
    if auth.has_membership(role='Admin'):
        #response.flash = 'hola mundo'
        #result = db(db.company_auth_user.id_user == auth.user.id).select(db.company_auth_user.ALL).first()
        result = db(db.auth_user.id == auth.user.id).select(db.auth_user.id_company).first().id_company #auth.user.id_company #db(db)
        #response.flash = result
        if result == 0:
            redirect(URL(c='default', f='wizard'))
        estatus_company = db.estatus[db(db.company_profile.id == result).select(db.company_profile.id_estatus).first().id_estatus].estatus
        if estatus_company == 'Disable':
            response.flash = 'Company is '+ estatus_company
            redirect(URL(c='default', f='dashboard')) 
    else:
        response.flash = 'User not active'           
    return dict()


@auth.requires_login()
def dashboard():
    if auth.has_membership(role = 'SuperAdmin'):
        #result = #db(db.company_profile.id_estatus == 'Disable').select()
        form = SQLFORM.grid(db.notify_superadmin, 
                       searchable=True,
                       details=True,
                       selectable=False,
                       deletable = True,
                       csv=False,
                       user_signature=False,
                       editable = False,
                       create= False
                       )
        if request.args(0) == 'view':
            id_company = int(request.args(2))
            panel_company = LOAD(c= 'default', f='manage_company.load', args=[id_company], ajax=True)
        else:
            panel_company = ''
        return dict(form = form, panel = panel_company)
    return dict(form = '', panel = '')



def manage_company():
    id_notify = request.args(0) or redirect(URL('default', 'dashboard'))
    #db(db.notify_superadmin.id == id_notify).select().first()
    id_company = db.notify_superadmin[id_notify].id_company
    #db.auth_membership.user_id.default = int(user_id)
    #db.auth_membership.user_id.writable = False
    db.company_profile.company_name.writable = False
    db.company_profile.id_estatus.writable = db.company_profile.id_estatus.readable = True
    form = SQLFORM(db.company_profile, id_company).process() #crud.update(db.company_profile, id_company)    
    if form.accepted:
        response.flash = 'Company Enable'
    else:
        #response.flash = form.vars.id_estatus
        id_estatus = form.vars.id_estatus
        if id_estatus:
            db(db.company_profile.id == id_company).update(id_estatus = id_estatus)
            #response.flash = 'Company Enable'
            response.flash = "Company status %s"%id_estatus
        #db(db.notify_superadmin)
        #del db.notify_superadmin[id_notify]

        #response.flash = 'Error'
    return dict(form = form)

@auth.requires_membership('Admin')
def company_register():
    #response.flash = T("Hello World")
    db.company_profile.id_user.default = auth.user.id
    #form = crud.create(db.company_profile)
    form = crud.create(db.company_profile) #SQLFORM(db.company_profile)
    if form.process().accepted:
        id_company = form.vars.id
        if db.company_auth_user.insert(id_company = id_company, id_user = auth.user.id, id_user_master = auth.user.id) > 0:
            response.flash = T('Company Create Success') 
            redirect(URL(c='default', f='index'))
    return dict(form = form)


@auth.requires_membership('Admin')
def wizard():
    STEPS = {
        0: ('type_company', 'company_name', 'physical_address', 'physical_city', 'physical_state', 'physical_zipcode', 'company_phone1', 'company_fax', 'company_website'),
        1: ('dispatch_name', 'mail_address', 'mail_city', 'mail_state', 'mail_zipcode','company_phone2', 'dispatch_email', 'timezone', 'operations_hours'),
        2: ('billing_contact', 'billing_address', 'billing_city', 'billing_state', 'billing_zipcode', 'billing_phone', 'billing_email', 'billing_fax'),
        3: ('dot_number', 'mc_number', 'company_registration'),
        4: ('insurance_company', 'insurance_agent','insurance_cargo', 'insurance_liability', 'insurance_agent_address', 'insurance_agent_city', 'insurance_agent_state', 'insurance_agent_zipcode', 'insurance_agent_email', 'insurance_agent_phone', 'id_user', 'date_register', 'id_estatus'),    
        #3: URL('done')
    }
    title = ('COMPANY INFORMATION', 'DISPATCH INFORMATION', 'BILLING INFORMATION', 'FMCSA INFORMATION', 'INSURANCE INFORMATION')
    step = int(request.args(0) or 0)
    if not step in STEPS:
        redirect(URL(args=0))
    fields = STEPS[step]

    try:
        db.company_profile.id_estatus.default = db(db.estatus.estatus == 'Disable').select(db.estatus.id).first().id
    except:
        pass
    
    if step == 0:
        session.wizard = {}
    if isinstance(fields, tuple):
        db.company_profile.id_user.default = auth.user.id
        form = SQLFORM.factory(*[f for f in db.company_profile if f.name in fields])

    if form.accepts(request, session):
        session.wizard.update(form.vars)
        if step < 4:
            redirect(URL(args = (step + 1)))
        else:
            id_company = db.company_profile.insert(**session.wizard)
            if id_company > 0:
                db(db.auth_user.id == auth.user.id).update(id_company = id_company)
                db.notify_superadmin.insert(id_company = id_company, id_user = auth.user.id, notify_title= 'the user %s has just registered a company: %s'%(auth.user.first_name+' - '+str(auth.user.id), session.wizard['company_name']), )
                response.flash = 'Company register success'
                redirect(URL(c='default', f='index'))

    #else:
     #   if len(session.wizard) > 0:
        #if form.accepts(request, session):
      #      id_company = db.company_profile.insert(**session.wizard)
       #     if id_company > 0:
        #        response.flash = 'Company register success'
        #redirect(fields)
        #session.wizard = {}
        #if db.company_auth_user.insert(id_company = id_company, id_user = auth.user.id, id_user_master = auth.user.id) > 0:
            #response.flash = T('Company Create Success') 
        #    redirect(URL(c='default', f='index'))
        

    return dict(form = form, step = step + 1, wizard = session.wizard, title=title[step])


# ---- API (example) -----
@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})

# ---- Smart Grid (example) -----
@auth.requires_membership('admin') # can only be accessed by members of admin groupd
def grid():
    response.view = 'generic.html' # use a generic view
    tablename = request.args(0)
    if not tablename in db.tables: raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[tablename], deletable=False, editable=False)
    return dict(grid=grid)

# ---- Embedded wiki (example) ----
def wiki():
    auth.wikimenu() # add the wiki to the menu
    return auth.wiki() 


def agregar_membresia(form):
    id_user = form.vars.id
    auth.add_membership('Admin', id_user)


# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    if request.args(0) == "profile":
        db.auth_user.auth_photo.readable = db.auth_user.auth_photo.writable = True

    auth.settings.register_onaccept = agregar_membresia
    return dict(form=auth())


# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
