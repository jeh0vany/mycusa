

@auth.requires_membership('Admin')
def index():
	company_id = db.auth_user[auth.user.id].id_company
	
	db.auth_user.auth_photo.readable = db.auth_user.auth_photo.writable = True
	db.auth_user.id_company.default = company_id
	form = SQLFORM.grid(db.auth_user.id_company == company_id,
		searchable=True,
		details=True,
		selectable=False,
		deletable = True,
		csv=False,
		user_signature=True
		)
	if request.args(0) == 'edit':
		id_user = int(request.args(2))
		membership_panel = LOAD(c= 'users', f='manage_membership.load', args=[id_user], ajax=True)
	else:
		membership_panel = ''
	return dict(users = form, menu = membership_panel)


@auth.requires_membership('Admin')
def add_user():
	company_id = db.auth_user[auth.user.id].id_company #db(db.auth_user.id == auth.user.id).select(db.auth_user.id_company).first().id_company
	menu = A('List Users', _href= URL(c= 'users', f = 'index'))
	db.auth_user.auth_photo.readable = db.auth_user.auth_photo.writable = True
	db.auth_user.id_company.default = company_id
	form = SQLFORM.grid(db.auth_user)
	if form.process().accepted:
		response.flash = 'New User Create'
		id_new_user = form.vars.id
		db.company_auth_user.insert(id_user = id_new_user, id_user_master = auth.user.id, id_company = company_id)
	return dict(form = form, menu = menu)

@auth.requires_membership('Admin')
def action_user():
	action = request.args(0)
	id_user = request.args(1)
	if action == 'edit':
		db.auth_user.auth_photo.readable = db.auth_user.auth_photo.writable = True
		form = crud.update(db.auth_user, id_user)
		membership_panel = LOAD(c= 'users', f='manage_membership.load',
                             args=[id_user],
                             ajax=True)
		return dict(form = form, membership_panel = membership_panel)

@auth.requires_membership('SuperAdmin')
def roles_users():
	form = SQLFORM.grid(db.auth_group,
                       searchable=False,
                       details=True,
                       selectable=False,
                       deletable = True,
                       csv=False,
                       user_signature=False)
	return dict(form = form)

def manage_membership():
    user_id = request.args(0) or redirect(URL('list_users'))
    db.auth_membership.user_id.default = int(user_id)
    db.auth_membership.user_id.writable = False
    form = SQLFORM.grid(db.auth_membership.user_id == user_id ,
                       args=[user_id],
                       searchable=False,
                       details=True,
                       selectable=False,
                       deletable = True,
                       csv=False,
                       user_signature=False)
    
    return form