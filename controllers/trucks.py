
@auth.requires_membership('Admin')
def index():
	id_company = db(db.auth_user.id == auth.user.id).select(db.auth_user.id_company).first().id_company
	db.truck.id_company.default = id_company
	db.truck.id_user.default = auth.user.id
	form = SQLFORM.grid(db.truck.id_company == id_company,
                       searchable=True,
                       details=True,
                       selectable=False,
                       deletable = True,
                       csv=False,
                       user_signature=False)
	return dict(form = form)
