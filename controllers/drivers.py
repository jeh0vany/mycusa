

@auth.requires_membership('Admin')
def index():
	id_company = db.auth_user[auth.user.id].id_company
	users_in_company = db(db.auth_user.id_company == id_company).select(db.auth_user.ALL)
	users_query = [[uic.id, uic.email+' - '+str(uic.id)] for uic in users_in_company]
	pos = 0
	for u_q in users_query:
		result = db(db.drivers.id_user_login == u_q[0]).select(db.drivers.id_user_login).first()
		if result:
			del users_query[pos]
		pos+=1 
	db.drivers.id_user.default = auth.user.id
	db.drivers.id_company.default = id_company
	db.drivers.id_user_login.requires = IS_IN_SET(users_query)
	form = SQLFORM.grid(db.drivers.id_company == id_company, searchable=True,
		details=True,
		selectable=False,
		deletable = True,
		csv=False,
		user_signature=False)
	return dict(form = form)


#def 

@auth.requires_membership('Admin')
def payment_info():
	
	id_company = db.auth_user[auth.user.id].id_company	
	db.driver_payment_info.id_company.default = id_company
	form = SQLFORM.grid(db.driver_payment_info.id_company == id_company,
		searchable=True,
		details=True,
		selectable=False,
		deletable = True,
		csv=False,
		user_signature=False)
	return dict(form = form)