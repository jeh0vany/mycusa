# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----

@auth.requires_membership('Admin')
def index():
	#response.flash = 'hoa'
	id_company = db(db.auth_user.id == auth.user.id).select(db.auth_user.id_company).first().id_company
	form = SQLFORM(db.company_profile, id_company).process()
	if form.accepted:
		response.flash = 'Company Update Record'
	return dict(form = form)

@auth.requires_membership('Admin')
def documents():
	id_company = db(db.auth_user.id == auth.user.id).select(db.auth_user.id_company).first().id_company
	#if request.args(0) == 'upload':
	db.company_documents.id_company.default = id_company
	db.company_documents.id_user.default = auth.user.id
	#form =  crud.create(db.company_documents)
	form = SQLFORM.grid(db.company_documents.id_company == id_company,
			searchable=True,
                       details=True,
                       selectable=False,
                       deletable = True,
                       csv=False,
                       user_signature=False)
	menu = A('View Documents', _href=URL(c='company', f='documents'), _title = 'View')
	return dict(result = form, menu = menu)
	#else:
	#	result = db(db.company_documents.id_company == id_company).select(db.company_documents.ALL, orderby = ~db.company_documents.date_create)
	#	menu = A('Upload Documents', _href=URL(c='company', f='documents', args = ['upload']), _title = 'Upload')
	#	return dict(result = result, menu = menu)

@auth.requires_membership('Admin')
def payments_method():
	form = SQLFORM.grid()
	return dict( form = form)


def action_document():
	action = request.args(0) or redirect(URL(c='company', f='documents'))
	id_document = request.args(1) or redirect(URL(c='company', f='documents'))
	if action == 'edit':
		form = crud.update(db.company_documents, id_document)
		return dict(form = form)
	elif action == 'delete':
		del db.company_documents[id_document]
		#db(db.company_documents == id_document).delete()
		redirect(URL(c='company', f='documents'))
		return dict()
