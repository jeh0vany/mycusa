# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# AppConfig configuration made easy. Look inside private/appconfig.ini
# Auth is for authenticaiton and access control
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth, Crud

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.15.5":
    raise HTTP(500, "Requires web2py 2.15.5 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
configuration = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(configuration.get('db.uri'),
             pool_size=configuration.get('db.pool_size'),
             migrate_enabled=configuration.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = [] 
if request.is_local and not configuration.get('app.production'):
    response.generic_patterns.append('*')

# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap4_inline'
response.form_label_separator = ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=configuration.get('host.names'))
crud = Crud(db)

# -------------------------------------------------------------------------
# create all tables needed by auth, maybe add a list of extra fields
# -------------------------------------------------------------------------
auth.settings.extra_fields['auth_user'] = [
    #Field('id_company', readable = False, writable = False),
    #Field('company_id', readable = False, writable = False),
    Field('auth_photo', 'upload', requires = IS_EMPTY_OR(IS_IMAGE()), readable = False, writable = False, label = "Photo"), 
    Field('id_company', 'integer', readable = False, writable = False, default = 0),
    Field('id_estatus', readable = False, writable = False),
]
auth.define_tables(username=False, signature=False)

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else configuration.get('smtp.server')
mail.settings.sender = configuration.get('smtp.sender')
mail.settings.login = configuration.get('smtp.login')
mail.settings.tls = configuration.get('smtp.tls') or False
mail.settings.ssl = configuration.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.settings.create_user_groups = False

# -------------------------------------------------------------------------  
# read more at http://dev.w3.org/html5/markup/meta.name.html               
# -------------------------------------------------------------------------
response.meta.author = configuration.get('app.author')
response.meta.description = configuration.get('app.description')
response.meta.keywords = configuration.get('app.keywords')
response.meta.generator = configuration.get('app.generator')
response.show_toolbar = configuration.get('app.toolbar')

# -------------------------------------------------------------------------
# your http://google.com/analytics id                                      
# -------------------------------------------------------------------------
response.google_analytics_id = configuration.get('google.analytics_id')

# -------------------------------------------------------------------------
# maybe use the scheduler
# -------------------------------------------------------------------------
if configuration.get('scheduler.enabled'):
    from gluon.scheduler import Scheduler
    scheduler = Scheduler(db, heartbeat=configuration.get('scheduler.heartbeat'))

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# company table
# -------------------------------------------------------------------------

db.define_table('type_company', 
        Field('type_company', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('description', 'text'),
        format = '%(type_company)s'
    )

db.define_table('country',
        Field('name', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('description', 'text'),
        format = '%(name)s'
    )

db.define_table('us_state',
        Field('id_country', db.country),
        Field('name', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('description', 'text'),
        format= '%(name)s'
    )

db.define_table('city', 
        Field('name', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('id_state', db.us_state),
        Field('description', 'text'),
        format = '%(name)s'
    )

db.define_table('estatus',
        Field('estatus', 'string', requires = IS_NOT_EMPTY()),
        Field('description', 'string'),
        format = '%(estatus)s - %(id)s'
    )

db.define_table('timezone',
        Field('timezone', requires = IS_NOT_EMPTY()),
        Field('description'),
        format = '%(timezone)s - %(id)s'
    )

db.define_table('company_profile',
        Field('type_company', db.type_company, label = 'Type of Company:' ),
        Field('company_name', 'string', label ='Company Name:', length = 125, requires = IS_NOT_EMPTY()),
        Field('physical_address', 'string', label ='Address:', length = 255,  requires = IS_NOT_EMPTY()),
        
        Field('physical_city', db.city, label ='City:'),
        Field('physical_state', db.us_state, label ='State:'),
        Field('physical_zipcode', 'string', label ='Zip Code:', length = 15, requires = IS_NOT_EMPTY()),
        Field('mail_address', 'string', label ='Address:', length = 255, requires = IS_NOT_EMPTY()),
        #Field('mail_city', 'string', db.city),
        
        Field('dispatch_name', 'string', length=255, label ='Contact Name:', requires = IS_NOT_EMPTY()),
        Field('dispatch_email', 'string', label ='Email:', length = 255, requires = [IS_NOT_EMPTY(), IS_EMAIL()]),

        Field('mail_state', db.us_state, label ='State:'),
        Field('mail_city', db.city, label ='City:'),
        Field('mail_zipcode', 'string', label ='Zip Code:', length = 15, requires = IS_NOT_EMPTY()),
        Field('billing_contact', 'string', label ='Contact Name:', length = 255, requires = IS_NOT_EMPTY()),
        Field('billing_address', 'string', label ='Address:', length = 255, requires = IS_NOT_EMPTY()),
        #
        Field('billing_state', db.us_state, label ='State:'),
        Field('billing_city', db.city, label ='City:'),
        Field('billing_zipcode', 'string', label ='Zip Code:', length = 15, requires = IS_NOT_EMPTY()),
        Field('billing_phone', 'string', label ='Phone:', length = 25, requires = IS_NOT_EMPTY()),
        Field('billing_email', 'string', label ='Email:', length= 255, requires = [IS_EMAIL(), IS_NOT_EMPTY()]),
        Field('billing_fax', 'string', label ='Fax:', length = 25),
       
        Field('bol_email', 'string', length = 255, requires = IS_EMPTY_OR(IS_EMAIL())),
        Field('invoice_email', 'string', length = 255, requires = IS_EMPTY_OR(IS_EMAIL())),#[IS_NOT_EMPTY(), IS_EMAIL()]),
        Field('company_phone1', 'string', label ='Phone:', length = 25, requires = IS_NOT_EMPTY()),
        Field('company_phone2', 'string', label ='Phone:', length = 25),
        Field('company_fax', 'string', length = 25),
        Field('contact_lastname', 'string', length = 125),
        Field('contact_firstname', 'string', length = 125),
        Field('company_website', 'string', length = 255),
        Field('dot_number', 'string', label ='Dot Number:', length = 255, requires = IS_NOT_EMPTY()),
        Field('mc_number', 'string', label ='MC Nunmber:', length = 255, requires = IS_NOT_EMPTY()),
        Field('established_year', 'integer'),
        Field('company_registration', 'integer', label ='Registration Year:', requires = IS_NOT_EMPTY()),
        Field('last_monthly_rating', 'decimal(3,2)'),
        Field('last_6months_rating', 'decimal(3,2)'),
        Field('actual_year_rating', 'decimal(3,2)'),
        Field('operations_hours', 'decimal(3,2)', label ='Operations Hours:'),
        Field('timezone', db.timezone, label ='Time Zone:'),
        Field('company_overview', 'string', length = 255),
        #Field('documents', 'upload'),
        Field('date_register', 'datetime', default = request.now, readable = False, writable = False),
        Field('id_user', db.auth_user, readable = False, writable = False),
        Field('insurance_company', length = 255, label ='Company Name:', requires = IS_NOT_EMPTY()),
        Field('insurance_agent', length = 255, label ='Contact Name:', requires = IS_NOT_EMPTY()),
        Field('insurance_cargo', requires = IS_IN_SET(['US$250,000.00','US$300,000.00','US$350,000.00','US$400,000.00','US$450,000.00','US$500,000.00',]), label = 'Insurance Cargo:'),
        Field('insurance_liability', requires = IS_IN_SET(['US$250,000.00','US$500,000.00','US$750,000.00','US$1,000,000.00',]), label = 'Insurance Liability:'),
        Field('insurance_agent_city', db.city, label ='City:'),
        Field('insurance_agent_state', db.us_state, label ='State:'),
        Field('insurance_agent_zipcode', length = 25, label ='Zip Code:', requires = IS_NOT_EMPTY()),
        Field('insurance_agent_email', length = 255, label ='Email:', requires = [IS_EMAIL(), IS_NOT_EMPTY()]),
        Field('insurance_agent_phone', length= 25, label ='Phone:', requires = IS_NOT_EMPTY()),
        Field('id_estatus', db.estatus, readable = False, writable = False),
        format = '%(company_name)s'
    )


db.define_table('company_documents',
        Field('id_company', db.company_profile, readable = False, writable = False),
        Field('id_user', db.auth_user, readable = False, writable = False),
        Field('date_create', 'datetime', default = request.now, readable = False, writable = False),
        Field('title', length = 255, requires = IS_NOT_EMPTY()),
        Field('documents', 'upload', label = 'Document', requires = IS_NOT_EMPTY()),
        Field('description', 'string', length = 255),
        format = '%(title)s'
    )

db.define_table('company_auth_user',
        Field('id_company', db.company_profile),
        Field('id_user', db.auth_user),
        Field('id_user_master', db.auth_user, readable = False, writable = False),
        format = '%(id_company)s - %(id_user_master)s'
    )

#db.define_table('insurance',
#        Field('company_name', 'string', length = 255, requires = IS_NOT_EMPTY()),
#        Field('agent_lastname', 'string', length = 125, requires = IS_NOT_EMPTY()),
#        Field('agent_firstname', 'string', length = 125, requires = IS_NOT_EMPTY()),
#        Field('address', 'string', length = 255, requires = IS_NOT_EMPTY()),
        #
 #       Field('us_state', db.us_state),
 #       Field('city', db.city),
 #       Field('zipcode', 'string', length = 15, requires = IS_NOT_EMPTY()),
 #       Field('email', 'string', length = 255, requires = [IS_NOT_EMPTY(), IS_EMAIL()]),
 #       Field('phone', 'string', length = 25, requires = IS_NOT_EMPTY()),
 #       Field('documents', 'upload'),
 #       format = '%(company_name)s - %(id)s'
  #  )

db.define_table('notify_superadmin',
        Field('id_user', db.auth_user, label = 'User Name'),
        Field('id_company', db.company_profile, readable = False, writable = False),
        #Field('id_superadmin', db.auth_user, readable = False, writable = False),
        Field('notify_title', 'string', length = 255, requires = IS_NOT_EMPTY(), label = 'Notification'),
        Field('date_register', 'datetime', default = request.now, readable = False, writable = False, label = 'Date Create' ),
        Field('ip_address', default = request.client, readable = False, writable = False),
        format = '%(notify)s'
    )

db.define_table('brands',
        Field('brand', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('logo', 'upload', requires = [IS_NOT_EMPTY(), IS_IMAGE()]),
        Field('description', 'string', length = 255),
        format = '%(brand)s - %(id)s'
    )

db.define_table('models',
        Field('id_brand', db.brands),
        Field('model', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('description', 'string', length = 255),
        format = '%(model)s - %(id)s'
    )

db.define_table('colors',
        Field('color', 'string', length = 125, requires = IS_NOT_EMPTY()),
        Field('description', 'string', length = 255),
        format = '%(color)s - %(id)s'
    )

db.define_table('capacity',
        Field('capacity', requires = IS_NOT_EMPTY()),
        Field('description'),
        format = '%(capacity)s'
    )

db.define_table('truck',
        Field('total_truck', 'string', readable = False, writable = False),
        Field('vin_number', 'string', length = 125),
        Field('year_truck', 'integer'),
        Field('brand', db.brands),
        Field('model', db.models),
        Field('color', db.colors),
        Field('trailer_capacity', db.capacity, label = 'Trailer Capacity:'),
        Field('picture', 'upload', requires = [IS_NOT_EMPTY(), IS_IMAGE()]),
        #format = '%()s'
        Field('id_company', db.company_profile, readable = False, writable = False),
        Field('id_user', db.auth_user, readable = False, writable = False),
        Field('date_create', 'datetime', default = request.now, readable = False, writable = False),
        format = '%(vin_number)s'
    )

#db.define_table('company_truck',
#        Field('id_company', db.company_profile),
#        Field('id_truck', db.truck),
#        format = 'Company %(id_company)s <--> %(id_truck)s'
#    )

db.define_table('sex',
        Field('sex', 'string', length = 15, requires = IS_NOT_EMPTY()),
        Field('abbreviation', 'string', length = 2, requires = IS_NOT_EMPTY()),
        Field('description', 'string', length = 255),
        format = '%(sex)s - %(id)s'
    )

db.define_table('drivers',
        Field('last_name', 'string', length = 125, requires = IS_NOT_EMPTY(), label = 'Last Name:'),
        Field('first_name', length = 125, requires = IS_NOT_EMPTY(), label = 'First Name:'),
        Field('address', 'string', length = 255, requires = IS_NOT_EMPTY(), label = 'Address:'),
        Field('us_state', db.us_state, label = 'State:'),
        Field('city', db.city, label = 'City:'),
        Field('zipcode', 'string', length = 15, requires = IS_NOT_EMPTY(), label = 'Zip Code:'),
        Field('phone', 'string', length = 25, requires = IS_NOT_EMPTY(),  label = 'Phone:'),
        Field('email', 'string', length = 255, requires = [IS_EMAIL(), IS_NOT_EMPTY()],  label = 'Email:'),
        Field('dob', 'string', length = 255,  label = 'DOB:'),
        Field('sex', db.sex,  label = 'Gender:'),
        Field('driver_license', 'string', length=35, requires = IS_NOT_EMPTY(), label = 'License Number:'),
        Field('driver_license_expdates', 'string', requires = IS_NOT_EMPTY(), label = 'License Exp. Date:'),
        Field('driver_twic', requires = IS_IN_SET(['Yes', 'Not']), label = 'Driver Twic:'),
        Field('cdl', 'string', length = 255, requires = IS_IN_SET(['Yes', 'Not']), label ='CDL:'),
        Field('payment_method', 'string', length = 125, requires = IS_IN_SET(['SALARY', 'COMMISION']), label = 'Payment Method:'),
        Field('date_register', 'datetime', default = request.now, readable = False, writable = False),
        Field('id_user', db.auth_user, readable = False, writable = False),
        Field('id_company', db.company_profile, readable = False, writable = False),
        Field('id_user_login', db.auth_user, label = 'Login As:'),
        format = '%(first_name)s'
    )

db.define_table('user_driver',
        
        Field('id_driver', db.drivers, label = 'Driver: '),
        Field('id_user', db.auth_user, label = 'Login as:'),
        Field('id_company', db.company_profile, readable = False, writable = False),
        Field('date_register', 'datetime', default = request.now, readable = False, writable = False),
        format = '%(id_user)s --> %(id_company)s'
    )

db.define_table('driver_payment_info',
        Field('driver_id', db.drivers, label = 'Driver:'),
        Field('entry_dates', 'datetime', requires = IS_DATETIME(), label = 'Entry Date:'),
        #Field('out_dates', 'datetime', requires = IS_DATETIME(), label = 'Out Date: '),
        Field('payment_method', 'string', length = 125, requires = IS_IN_SET(['SALARY', 'COMMISION']), label = 'Payment Method: '),
        Field('id_company', db.company_profile, readable = False, writable = False),
        Field('date_register', 'datetime', readable = False, writable = False, default = request.now),
        format = '%(driver_id)s %(payment_method)s'
    )

db.define_table('company_rating',
        Field('id_company', db.company_profile, readable = False, writable = False),
        Field('truck_id', db.truck),
        Field('pickup_rating'),
        Field('drop_rating'),
    )

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)

db.company_profile.company_name.requires = IS_NOT_IN_DB(db, 'company_profile.company_name')
db.company_profile.company_phone1.requires = IS_NOT_IN_DB(db, 'company_profile.company_phone1')
db.company_profile.company_phone2.requires = IS_EMPTY_OR(IS_NOT_IN_DB(db, 'company_profile.company_phone2'))
db.company_profile.company_fax.requires = IS_EMPTY_OR(IS_NOT_IN_DB(db, 'company_profile.company_fax'))
db.company_profile.dispatch_email.requires = IS_NOT_IN_DB(db, 'company_profile.dispatch_email')
